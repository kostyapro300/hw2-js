
// 1) Типи даних в JavaScript:
// number: числа
// string: рядки (текст)
// boolean: логічні значення true або false
// null: відсутність значення
// undefined: значення не визначено
// object: об'єкти
// BigInt: цілі числа довільної точності


// 2) Разница между == и ===

// ==(нестрогая рівність) Порівнює значення зі звільненням від різниці типів.
// === (строге рівність): Порівнює значення та типи без звільнення від різниці типів. 

// 3) Оператор це --
// символ або фраза, яка вказує комп'ютеру виконати певну дію. 


// Практическое задание ---------------------

let name = prompt("Здравствуйте, как вас звать?");

while (!name || name === '') {
    name = prompt("Пожалуйста, введите ваше имя:");
}

let years = prompt("Введите ваш возраст:");

while (isNaN(years)) {
    years = prompt("Пожалуйста, введите ваш возраст:");
}

if (years < 18) {
    alert("You are not allowed to visit this website");
} else if (years >= 18 && years <= 22) {
    let confirmation = confirm("Are you sure you want to continue?");
    if (confirmation) {
        alert("Welcome, " + name);
    } else {
        alert("You are not allowed to visit this website");
    }
} else {
    alert("Welcome, " + name);
}

